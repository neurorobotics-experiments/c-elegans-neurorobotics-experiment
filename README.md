# C Elegans Neurorobotics Experiment



## Data from the neurorobot with the C elegans connectome

This repository contains the data of the neural dynamics and their relation with the emergent actions of a robotic vehicle that is controlled by a neural network numerical simulation based on the nervous system of the nematode Caenorhabditis elegans. The robot interacts with the environment through a sensor that transmits the information to sensory neurons, while motor neurons outputs are connected to wheels.

## The robotic model
We use a robot design that essentially consists of a vehicle with two lateral motors connected to wheels and a distance sensor in the front. This allows the vehicle to sense the environment and move on the ground.
We used two vehicles for our experiments. One of the robots is the commercially available GoPiGo robot by Dexter Industries. The software that controls this robot is open source and can be downloaded from https://www.dexterindustries.com/raspberry-pi-robot-software/. This allows for a straightforward reproduction of our experiments. Since the hardware of the GoPiGO robot is not open, we decided to build a custom robot with a similar design that can easily be constructed with off the shelf components at a low cost. In this way we provide an open hardware and open software alternative to reproduce the experiments.
The numerical simulation was run in a Raspberry Pi (Foundation, 2020) computer mounted in the vehicles, that also interfaced with the distance sensor and the motor control boards.
